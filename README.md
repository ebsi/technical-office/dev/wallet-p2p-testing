# EBSI Wallet Peer-to-peer Testing

Together EBSI and the wallet providers form an ecosystem of actors. Interopebility between these actors improves the viability of the ecosystem as a whole. EBSI provides comprehensive conformance testing facilities for various capabilities of wallets, including those for individuals and organisations. These tests cover numerous scenarios, such as accreditation and authorisation, issuance, verification, and the request and presentation of credentials. As these scenarios involve multiple actors, the conformance test examines each scenario from different viewpoints. Successfully passing the conformance tests ensures a high level of conformity and compatibility among the different wallets. However, it's important to note that conformance testing has its limitations, and real-world compatibility issues may still arise between two compliant wallets. Often, these issues stem from subtle differences in wallet implementations. Therefore, we encourage wallet providers to engage in collaborative testing with other providers to enhance compatibility in practical use.

To facilitate testing between wallet providers, we present a directory of different conformant wallets and their providers. In addition to testing other wallets independently we encourage providers to contact one another to report issues, provide feedback and to coordinate testing in public or closed test environments.

## Process
If you wish to update your wallet's information in this directory, please contact EBSI Help Desk https://ec.europa.eu/digital-building-blocks/wikis/display/EBSI/Help+Desk.

## Organisation Wallets
### Example Wallet

| Field            | Value                                                                                                             |
|------------------|-------------------------------------------------------------------------------------------------------------------|
| Name             | _Example wallet_                                                                                                  |
| Provider         | _Example provider_                                                                                                |
| Application type | _web, mobile_                                                                                                     |
| Capabilities     | Accredit & Authorise v3, Issue v3, Verify v3                                                                      |
| Website          | _https://www.example.com_                                                                                         |
| Well-Known URIs  | Production _https://www.example.com/.well-known/example_, Test _https://www.example.com/.well-known/example-test_ |
| Contact          | _info@example.com_                                                                                                |
| Notes            | _We have verified interoperability with the following wallets: Foo Wallet v1.0, Bar Wallet v2.0_                  |


## Individual Wallets
### Example wallet

| Field            | Value                                                                                                             |
|------------------|-------------------------------------------------------------------------------------------------------------------|
| Name             | _Example wallet_                                                                                                  |
| Provider         | _Example provider_                                                                                                |
| Application type | _web, mobile_                                                                                                     |
| Capabilities     | Request & Present                                                                                                 |
| Website          | _https://www.example.com_                                                                                         |
| Contact          | _info@example.com_                                                                                                |
| Notes            | _We have verified interoperability with the following wallets: Foo Wallet v1.0, Bar Wallet v2.0_                            |

